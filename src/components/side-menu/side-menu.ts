import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ClientOverviewPage } from '../../pages/fysio/client-overview/client-overview';
import { ProfilePage } from '../../pages/fysio/profile/profile';
import { AboutPage } from '../../pages/fysio/about/about';
import { NotificationsPage } from '../../pages/fysio/notifications/notifications';

/**
 * Generated class for the SideMenuComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'side-menu',
  templateUrl: 'side-menu.html'
})
export class SideMenuComponent {

  // text: string;

  constructor(public navCtrl: NavController) {
    // console.log('Hello SideMenuComponent Component');
    // this.text = 'Hello World';
  }

  goClientPage() {
    this.navCtrl.setRoot(ClientOverviewPage);
  }

  goProfilePage() {
    this.navCtrl.setRoot(ProfilePage);
  }

  goAboutPage() {
    this.navCtrl.setRoot(AboutPage);
  }

  goNotifPage() {
    this.navCtrl.setRoot(NotificationsPage);
  }

}
