import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SideMenuComponent } from './side-menu/side-menu';
import { SideMenuSeniorComponent } from './side-menu-senior/side-menu-senior';
@NgModule({
	declarations: [SideMenuComponent,
    SideMenuSeniorComponent],
	imports: [BrowserModule],
	exports: [SideMenuComponent,
    SideMenuSeniorComponent]
})
export class ComponentsModule {}
