import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

import { HomePage } from '../../pages/senior/home/home';
import { SchedulePage } from '../../pages/senior/schedule/schedule';
import { SeniorProfilePage } from '../../pages/senior/senior-profile/senior-profile';

/**
 * Generated class for the SideMenuSeniorComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'side-menu-senior',
  templateUrl: 'side-menu-senior.html'
})
export class SideMenuSeniorComponent {

  currentPage:any;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
    this.currentPage = this.viewCtrl.name;
  }

  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

  goSchedule() {
    this.navCtrl.setRoot(SchedulePage);
  }

  profilePage() {
    this.navCtrl.setRoot(SeniorProfilePage);
  }

}
