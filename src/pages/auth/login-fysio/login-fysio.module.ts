import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginFysioPage } from './login-fysio';

@NgModule({
  declarations: [
    LoginFysioPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginFysioPage),
  ],
})
export class LoginFysioPageModule {}
