import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { LoginSeniorPage } from '../login-senior/login-senior';
import { ModalForgotPasswordPage } from '../modal-forgot-password/modal-forgot-password';
import { PersonalInfoPage } from '../../fysio/personal-info/personal-info';

/**
 * Generated class for the LoginFysioPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-fysio',
  templateUrl: 'login-fysio.html',
})
export class LoginFysioPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  login() {
    this.navCtrl.setRoot(PersonalInfoPage);
  }

  seniorLogin() {
    this.navCtrl.setRoot(LoginSeniorPage);
  }

  showForgotPassword() {
    let modal = this.modalCtrl.create(ModalForgotPasswordPage);
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginFysioPage');
  }

}
