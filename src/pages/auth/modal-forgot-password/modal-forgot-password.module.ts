import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalForgotPasswordPage } from './modal-forgot-password';

@NgModule({
  declarations: [
    ModalForgotPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalForgotPasswordPage),
  ],
})
export class ModalForgotPasswordPageModule {}
