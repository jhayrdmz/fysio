import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalForgotPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-forgot-password',
  templateUrl: 'modal-forgot-password.html',
})
export class ModalForgotPasswordPage {

  constructor(public viewCtrl: ViewController) {
  }

  modalClose() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalForgotPasswordPage');
  }

}
