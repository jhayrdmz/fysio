import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginSeniorPage } from './login-senior';

@NgModule({
  declarations: [
    LoginSeniorPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginSeniorPage),
  ],
})
export class LoginSeniorPageModule {}
