import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { HomePage } from '../../senior/home/home';
import { LoginFysioPage } from '../login-fysio/login-fysio';
import { ModalForgotPasswordPage } from '../modal-forgot-password/modal-forgot-password';
import { ActivateCodePage } from '../../senior/activate-code/activate-code';

/**
 * Generated class for the LoginSeniorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-senior',
  templateUrl: 'login-senior.html',
})
export class LoginSeniorPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  activateCode() {
    this.navCtrl.setRoot(ActivateCodePage);
  }

  fysioLogin() {
    this.navCtrl.setRoot(LoginFysioPage);
  }

  showForgotPassword() {
    let modal = this.modalCtrl.create(ModalForgotPasswordPage);
    modal.present();
  }

  goLogin() {
    this.navCtrl.setRoot(HomePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginSeniorPage');
  }

}
