import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { AddAppointmentPage } from '../add-appointment/add-appointment';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { ChangeProgramPage } from '../change-program/change-program';
import { ChangeWalkProgramPage } from '../change-walk-program/change-walk-program';
import { OverviewTrainingPage } from '../overview-training/overview-training';

/**
 * Generated class for the ClientDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-detail',
  templateUrl: 'client-detail.html',
})
export class ClientDetailPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  changeWalkProgram() {
    let modal = this.modalCtrl.create(ChangeWalkProgramPage);
    modal.present();
  }

  changeProgram() {
    this.navCtrl.setRoot(ChangeProgramPage);
  }

  editProfile() {
    this.navCtrl.setRoot(EditProfilePage);
  }

  addAppointment() {
    let modal = this.modalCtrl.create(AddAppointmentPage);
    modal.present();
  }

  overviewTraining() {
    this.navCtrl.setRoot(OverviewTrainingPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientDetailPage');
  }

}
