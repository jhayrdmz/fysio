import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { EditGoalPage } from '../edit-goal/edit-goal';

/**
 * Generated class for the EditProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  birthdate: '1954-09-16';
  gender: 'm';

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  editGoal() {
    this.navCtrl.push(EditGoalPage);
  }

  sampleError() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Are you sure you want to remove?',
      cssClass: 'alert-custom',
      buttons: [
          {
              text: 'Cancel',
              cssClass: 'btn btn-cancel'
          },
          {
              text: 'Delete account',
              cssClass: 'btn btn-secondary'
          }
      ]
    });
    alert.present();
  }

  profilePage() {
    this.navCtrl.setRoot(ProfilePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

}
