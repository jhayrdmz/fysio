import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';

/**
 * Generated class for the ModalEmailClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-email-client',
  templateUrl: 'modal-email-client.html',
})
export class ModalEmailClientPage {

  constructor(public events: Events) {
  }

  closeParentModal() {
    this.events.publish('modal:closed');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalEmailClientPage');
  }

}
