import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEmailClientPage } from './modal-email-client';

@NgModule({
  declarations: [
    ModalEmailClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEmailClientPage),
  ],
})
export class ModalEmailClientPageModule {}
