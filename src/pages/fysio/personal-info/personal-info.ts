import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TourPage } from '../tour/tour';
import { LoginFysioPage } from '../../auth/login-fysio/login-fysio';

/**
 * Generated class for the PersonalInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-personal-info',
  templateUrl: 'personal-info.html',
})
export class PersonalInfoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  loginPage() {
    this.navCtrl.setRoot(LoginFysioPage);
  }

  continue() {
    this.navCtrl.setRoot(TourPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PersonalInfoPage');
  }

}
