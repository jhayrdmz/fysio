import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalChooseGoalPage } from './modal-choose-goal';

@NgModule({
  declarations: [
    ModalChooseGoalPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalChooseGoalPage),
  ],
})
export class ModalChooseGoalPageModule {}
