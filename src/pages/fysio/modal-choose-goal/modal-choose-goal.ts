import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalChooseProgramPage } from '../modal-choose-program/modal-choose-program';

/**
 * Generated class for the ModalChooseGoalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-choose-goal',
  templateUrl: 'modal-choose-goal.html',
})
export class ModalChooseGoalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  chooseProgram() {
    this.navCtrl.setRoot(ModalChooseProgramPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalChooseGoalPage');
  }

}
