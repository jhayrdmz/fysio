import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { ModalChooseGoalPage } from '../modal-choose-goal/modal-choose-goal';

/**
 * Generated class for the ModalNewClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-new-client',
  templateUrl: 'modal-new-client.html',
})
export class ModalNewClientPage {

  constructor(public navCtrl: NavController ) {
  }

  chooseGoal() {
    this.navCtrl.setRoot(ModalChooseGoalPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalNewClientPage');
  }

}
