import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalNewClientPage } from './modal-new-client';

@NgModule({
  declarations: [
    ModalNewClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalNewClientPage),
  ],
})
export class ModalNewClientPageModule {}
