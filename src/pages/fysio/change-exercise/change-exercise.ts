import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';

/**
 * Generated class for the ChangeExercisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-exercise',
  templateUrl: 'change-exercise.html',
})
export class ChangeExercisePage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {
  }

  modalClose() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeExercisePage');
  }

}
