import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeExercisePage } from './change-exercise';

@NgModule({
  declarations: [
    ChangeExercisePage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeExercisePage),
  ],
})
export class ChangeExercisePageModule {}
