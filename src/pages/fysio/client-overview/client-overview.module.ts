import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientOverviewPage } from './client-overview';

@NgModule({
  declarations: [
    ClientOverviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ClientOverviewPage),
  ],
})
export class ClientOverviewPageModule {}
