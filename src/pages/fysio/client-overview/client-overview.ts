import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, AlertController } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { ClientDetailPage } from '../client-detail/client-detail';
import { ModalClientPage } from '../modal-client/modal-client';

/**
 * Generated class for the ClientOverviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-overview',
  templateUrl: 'client-overview.html',
})
export class ClientOverviewPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public alertCtrl: AlertController) {
  }

  goClientDetail() {
    this.navCtrl.setRoot(ClientDetailPage);
  }

  addClient() {
    let modal = this.modalCtrl.create(ModalClientPage);
    modal.present();
    // let alert = this.alertCtrl.create({
    //   title: 'New Friend!',
    //   subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
    //   cssClass: 'alert-custom',
    //   buttons: ['OK']
    // });
    // alert.present();
  }

  goProfilePage() {
    this.navCtrl.setRoot(ProfilePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientOverviewPage');
  }

}
