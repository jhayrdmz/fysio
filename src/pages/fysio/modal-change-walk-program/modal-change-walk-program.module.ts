import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalChangeWalkProgramPage } from './modal-change-walk-program';

@NgModule({
  declarations: [
    ModalChangeWalkProgramPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalChangeWalkProgramPage),
  ],
})
export class ModalChangeWalkProgramPageModule {}
