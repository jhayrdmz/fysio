import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalControlProgramPage } from '../modal-control-program/modal-control-program';

/**
 * Generated class for the ModalChangeWalkProgramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-change-walk-program',
  templateUrl: 'modal-change-walk-program.html',
})
export class ModalChangeWalkProgramPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  controlProgram() {
    this.navCtrl.setRoot(ModalControlProgramPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalChangeWalkProgramPage');
  }

}
