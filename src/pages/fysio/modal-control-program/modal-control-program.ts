import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalEmailClientPage } from '../modal-email-client/modal-email-client';

/**
 * Generated class for the ModalControlProgramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-control-program',
  templateUrl: 'modal-control-program.html',
})
export class ModalControlProgramPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  emailClient() {
    this.navCtrl.setRoot(ModalEmailClientPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalControlProgramPage');
  }

}
