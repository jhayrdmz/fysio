import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalControlProgramPage } from './modal-control-program';

@NgModule({
  declarations: [
    ModalControlProgramPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalControlProgramPage),
  ],
})
export class ModalControlProgramPageModule {}
