import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeWalkProgramPage } from './change-walk-program';

@NgModule({
  declarations: [
    ChangeWalkProgramPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeWalkProgramPage),
  ],
})
export class ChangeWalkProgramPageModule {}
