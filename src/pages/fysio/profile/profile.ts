import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginFysioPage } from '../../auth/login-fysio/login-fysio';
import { ProfileSettingsPage } from '../profile-settings/profile-settings';

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  logout() {
    this.navCtrl.setRoot(LoginFysioPage);
  }

  editProfile() {
    this.navCtrl.setRoot(ProfileSettingsPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
