import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { AddExercisePage } from '../add-exercise/add-exercise';
import { ChangeExercisePage } from '../change-exercise/change-exercise';
/**
 * Generated class for the ChangeProgramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-program',
  templateUrl: 'change-program.html',
})
export class ChangeProgramPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  changeExercise() {
    let modal = this.modalCtrl.create(ChangeExercisePage);
    modal.present();
  }

  addExercise() {
    let modal = this.modalCtrl.create(AddExercisePage);
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeProgramPage');
  }

}
