import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';

import { ClientOverviewPage } from '../client-overview/client-overview';

/**
 * Generated class for the TourPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tour',
  templateUrl: 'tour.html',
})
export class TourPage {

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController) {
  }

  skipPage() {
    this.navCtrl.setRoot(ClientOverviewPage);
  }

  slideNext() {
    this.slides.slideNext();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TourPage');
  }

}
