import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalChangeProgramPage } from './modal-change-program';

@NgModule({
  declarations: [
    ModalChangeProgramPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalChangeProgramPage),
  ],
})
export class ModalChangeProgramPageModule {}
