import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalChangeWalkProgramPage } from '../modal-change-walk-program/modal-change-walk-program';

/**
 * Generated class for the ModalChangeProgramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-change-program',
  templateUrl: 'modal-change-program.html',
})
export class ModalChangeProgramPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  changeWalkProgram() {
    this.navCtrl.setRoot(ModalChangeWalkProgramPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalChangeProgramPage');
  }

}
