import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalChangeProgramPage } from '../modal-change-program/modal-change-program';

/**
 * Generated class for the ModalChooseProgramPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-choose-program',
  templateUrl: 'modal-choose-program.html',
})
export class ModalChooseProgramPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  changeProgram() {
    this.navCtrl.setRoot(ModalChangeProgramPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalChooseProgramPage');
  }

}
