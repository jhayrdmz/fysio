import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalChooseProgramPage } from './modal-choose-program';

@NgModule({
  declarations: [
    ModalChooseProgramPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalChooseProgramPage),
  ],
})
export class ModalChooseProgramPageModule {}
