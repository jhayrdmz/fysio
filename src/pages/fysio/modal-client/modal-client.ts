import { Component } from '@angular/core';
import { Events, IonicPage, NavController, ViewController } from 'ionic-angular';

import { ModalNewClientPage } from '../modal-new-client/modal-new-client';

/**
 * Generated class for the ModalClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-client',
  templateUrl: 'modal-client.html',
})
export class ModalClientPage {

  rootModal:any = ModalNewClientPage;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public events: Events) {
    events.subscribe('modal:closed', function() {
      viewCtrl.dismiss();
    });
  }

  modalClose() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalClientPage');
  }

}
