import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OverviewTrainingPage } from './overview-training';

@NgModule({
  declarations: [
    OverviewTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(OverviewTrainingPage),
  ],
})
export class OverviewTrainingPageModule {}
