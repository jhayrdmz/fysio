import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TrainingWarmupPage } from '../training-warmup/training-warmup';

/**
 * Generated class for the TrainingCheckinPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-checkin',
  templateUrl: 'training-checkin.html',
})
export class TrainingCheckinPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goWarmpup() {
    this.navCtrl.setRoot(TrainingWarmupPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingCheckinPage');
  }

}
