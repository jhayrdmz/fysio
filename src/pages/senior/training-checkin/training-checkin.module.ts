import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingCheckinPage } from './training-checkin';

@NgModule({
  declarations: [
    TrainingCheckinPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingCheckinPage),
  ],
})
export class TrainingCheckinPageModule {}
