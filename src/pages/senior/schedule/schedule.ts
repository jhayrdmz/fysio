import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';

import { TrainingPage } from '../training/training';
import { HomePage } from '../home/home';

/**
 * Generated class for the SchedulePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
  }

  startTraining() {
    let modal = this.modalCtrl.create(TrainingPage);
    modal.present();
  }

  goBack() {
    this.navCtrl.setRoot(HomePage);
  }

  restday() {
    //
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulePage');
  }

}
