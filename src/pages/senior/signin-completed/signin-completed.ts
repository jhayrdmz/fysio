import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import { SigninPlanningPage } from '../signin-planning/signin-planning';
import { SigninPersonalInfoTwoPage } from '../signin-personal-info-two/signin-personal-info-two';

/**
 * Generated class for the SigninCompletedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin-completed',
  templateUrl: 'signin-completed.html',
})
export class SigninCompletedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goSigninPlanning() {
    this.navCtrl.setRoot(SigninPlanningPage);
  }

  goPersonalInfo() {
    this.navCtrl.setRoot(SigninPersonalInfoTwoPage);
  }
  
  goHome() {
    this.navCtrl.setRoot(HomePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninCompletedPage');
  }

}
