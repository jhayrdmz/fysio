import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninCompletedPage } from './signin-completed';

@NgModule({
  declarations: [
    SigninCompletedPage,
  ],
  imports: [
    IonicPageModule.forChild(SigninCompletedPage),
  ],
})
export class SigninCompletedPageModule {}
