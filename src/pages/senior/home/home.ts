import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ModalController } from 'ionic-angular';

import { AllChallengesPage } from '../all-challenges/all-challenges';
import { ExplanationModalPage } from '../explanation-modal/explanation-modal';
import { SchedulePage } from '../schedule/schedule';

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  allChallenges() {
    this.navCtrl.setRoot(AllChallengesPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  slideNext() {
    this.slides.slideNext();
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  goSchedule() {
    this.navCtrl.setRoot(SchedulePage);
  }

  explanation() {
    let modal = this.modalCtrl.create(ExplanationModalPage, null, {
      cssClass: 'full-screen-modal'
    });
    modal.present();
  }

}
