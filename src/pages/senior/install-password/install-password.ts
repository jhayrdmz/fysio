import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ActivateCodePage } from '../activate-code/activate-code';
import { SigninWelcomePage } from '../signin-welcome/signin-welcome';

/**
 * Generated class for the InstallPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-install-password',
  templateUrl: 'install-password.html',
})
export class InstallPasswordPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  activateCode() {
    this.navCtrl.setRoot(ActivateCodePage);
  }

  signinWelcome() {
    this.navCtrl.setRoot(SigninWelcomePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstallPasswordPage');
  }

}
