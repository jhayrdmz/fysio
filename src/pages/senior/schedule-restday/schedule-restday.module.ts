import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduleRestdayPage } from './schedule-restday';

@NgModule({
  declarations: [
    ScheduleRestdayPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleRestdayPage),
  ],
})
export class ScheduleRestdayPageModule {}
