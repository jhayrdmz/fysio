import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingStartPage } from './training-start';

@NgModule({
  declarations: [
    TrainingStartPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingStartPage),
  ],
})
export class TrainingStartPageModule {}
