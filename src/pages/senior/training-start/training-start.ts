import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TrainingCheckinPage } from '../training-checkin/training-checkin';

/**
 * Generated class for the TrainingStartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-start',
  templateUrl: 'training-start.html',
})
export class TrainingStartPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goCheckin() {
    this.navCtrl.setRoot(TrainingCheckinPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingStartPage');
  }

}
