import { Component } from '@angular/core';
import { Events, IonicPage, NavController, ViewController } from 'ionic-angular';

import { TrainingStartPage } from '../training-start/training-start';
// https://marvelapp.com/28d687j/screen/31786797

import { TrainingCheckinPage } from '../training-checkin/training-checkin';
// https://marvelapp.com/28d687j/screen/31786795
//https://marvelapp.com/28d687j/screen/31786798

import { TrainingWarmupPage } from '../training-warmup/training-warmup';
// https://marvelapp.com/28d687j/screen/31786796

import { TrainingBalancePage } from '../training-balance/training-balance';
// https://marvelapp.com/28d687j/screen/31786777

import { TrainingEndPage } from '../training-end/training-end';
// https://marvelapp.com/28d687j/screen/31786838

import { TrainingNextPage } from '../training-next/training-next';
// https://marvelapp.com/28d687j/screen/31786812

/**
 * Generated class for the TrainingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training',
  templateUrl: 'training.html',
})
export class TrainingPage {

  rootModal = TrainingStartPage;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public events: Events) {
    events.subscribe('modal:closed', function() {
      viewCtrl.dismiss();
    });
  }

  modalClose() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingPage');
  }

}
