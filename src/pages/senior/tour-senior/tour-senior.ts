import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';

/**
 * Generated class for the TourSeniorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tour-senior',
  templateUrl: 'tour-senior.html',
})
export class TourSeniorPage {

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController) {
  }

  slideNext() {
    this.slides.slideNext();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TourSeniorPage');
  }

}
