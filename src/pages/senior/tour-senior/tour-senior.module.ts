import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourSeniorPage } from './tour-senior';

@NgModule({
  declarations: [
    TourSeniorPage,
  ],
  imports: [
    IonicPageModule.forChild(TourSeniorPage),
  ],
})
export class TourSeniorPageModule {}
