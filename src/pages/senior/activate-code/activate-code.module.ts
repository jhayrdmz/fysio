import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivateCodePage } from './activate-code';

@NgModule({
  declarations: [
    ActivateCodePage,
  ],
  imports: [
    IonicPageModule.forChild(ActivateCodePage),
  ],
})
export class ActivateCodePageModule {}
