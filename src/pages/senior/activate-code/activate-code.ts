import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InstallPasswordPage } from '../install-password/install-password';
import { LoginSeniorPage } from '../../auth/login-senior/login-senior';

/**
 * Generated class for the ActivateCodePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activate-code',
  templateUrl: 'activate-code.html',
})
export class ActivateCodePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  loginPage() {
    this.navCtrl.setRoot(LoginSeniorPage);
  }

  installPass() {
    this.navCtrl.setRoot(InstallPasswordPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivateCodePage');
  }

}
