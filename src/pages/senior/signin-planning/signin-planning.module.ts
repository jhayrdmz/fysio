import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninPlanningPage } from './signin-planning';

@NgModule({
  declarations: [
    SigninPlanningPage,
  ],
  imports: [
    IonicPageModule.forChild(SigninPlanningPage),
  ],
})
export class SigninPlanningPageModule {}
