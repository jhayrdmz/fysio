import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SigninPersonalInfoTwoPage } from '../signin-personal-info-two/signin-personal-info-two';
import { SigninCompletedPage } from '../signin-completed/signin-completed';

/**
 * Generated class for the SigninPlanningPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin-planning',
  templateUrl: 'signin-planning.html',
})
export class SigninPlanningPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goSigninCompleted() {
    this.navCtrl.setRoot(SigninCompletedPage);
  }

  goSigninPersonalInfo() {
    this.navCtrl.setRoot(SigninPersonalInfoTwoPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPlanningPage');
  }

}
