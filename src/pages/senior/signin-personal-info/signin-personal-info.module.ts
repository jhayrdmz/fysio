import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninPersonalInfoPage } from './signin-personal-info';

@NgModule({
  declarations: [
    SigninPersonalInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(SigninPersonalInfoPage),
  ],
})
export class SigninPersonalInfoPageModule {}
