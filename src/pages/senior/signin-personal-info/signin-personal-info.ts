import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SigninWelcomePage } from '../signin-welcome/signin-welcome';
import { SigninPersonalInfoTwoPage } from '../signin-personal-info-two/signin-personal-info-two';

/**
 * Generated class for the SigninPersonalInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin-personal-info',
  templateUrl: 'signin-personal-info.html',
})
export class SigninPersonalInfoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goSigninWelcome() {
    this.navCtrl.setRoot(SigninWelcomePage);
  }

  goSigninPersonalTwo() {
    this.navCtrl.setRoot(SigninPersonalInfoTwoPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPersonalInfoPage');
  }

}
