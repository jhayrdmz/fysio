import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingBalancePage } from './training-balance';

@NgModule({
  declarations: [
    TrainingBalancePage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingBalancePage),
  ],
})
export class TrainingBalancePageModule {}
