import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TrainingEndPage } from '../training-end/training-end';

/**
 * Generated class for the TrainingBalancePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-balance',
  templateUrl: 'training-balance.html',
})
export class TrainingBalancePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

	goCheckOut() {
		this.navCtrl.setRoot(TrainingEndPage);
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingBalancePage');
  }

}
