import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeniorEditProfilePage } from './senior-edit-profile';

@NgModule({
  declarations: [
    SeniorEditProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(SeniorEditProfilePage),
  ],
})
export class SeniorEditProfilePageModule {}
