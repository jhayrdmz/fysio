import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { SeniorProfilePage } from '../senior-profile/senior-profile';

/**
 * Generated class for the SeniorEditProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-senior-edit-profile',
  templateUrl: 'senior-edit-profile.html',
})
export class SeniorEditProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  profilePage() {
    this.navCtrl.setRoot(SeniorProfilePage);
  }

  sampleError() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Are you sure you want to remove?',
      cssClass: 'alert-custom',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'btn btn-cancel'
        },{
          text: 'Delete account',
          cssClass: 'btn btn-secondary'
        }]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeniorEditProfilePage');
  }

}
