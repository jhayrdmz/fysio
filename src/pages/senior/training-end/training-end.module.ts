import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingEndPage } from './training-end';

@NgModule({
  declarations: [
    TrainingEndPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingEndPage),
  ],
})
export class TrainingEndPageModule {}
