import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingNextPage } from './training-next';

@NgModule({
  declarations: [
    TrainingNextPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingNextPage),
  ],
})
export class TrainingNextPageModule {}
