import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExplanationModalPage } from './explanation-modal';

@NgModule({
  declarations: [
    ExplanationModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ExplanationModalPage),
  ],
})
export class ExplanationModalPageModule {}
