import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainingWarmupPage } from './training-warmup';

@NgModule({
  declarations: [
    TrainingWarmupPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainingWarmupPage),
  ],
})
export class TrainingWarmupPageModule {}
