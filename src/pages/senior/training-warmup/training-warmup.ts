import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TrainingBalancePage } from '../training-balance/training-balance';

/**
 * Generated class for the TrainingWarmupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training-warmup',
  templateUrl: 'training-warmup.html',
})
export class TrainingWarmupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goBalance() {
  	this.navCtrl.setRoot(TrainingBalancePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingWarmupPage');
  }

}
