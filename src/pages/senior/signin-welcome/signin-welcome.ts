import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InstallPasswordPage } from '../install-password/install-password';
import { SigninPersonalInfoPage } from '../signin-personal-info/signin-personal-info';

/**
 * Generated class for the SigninWelcomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin-welcome',
  templateUrl: 'signin-welcome.html',
})
export class SigninWelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goInstallPass() {
    this.navCtrl.setRoot(InstallPasswordPage);
  }

  signinPersonalInfo() {
    this.navCtrl.setRoot(SigninPersonalInfoPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninWelcomePage');
  }

}
