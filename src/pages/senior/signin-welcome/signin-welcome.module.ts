import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninWelcomePage } from './signin-welcome';

@NgModule({
  declarations: [
    SigninWelcomePage,
  ],
  imports: [
    IonicPageModule.forChild(SigninWelcomePage),
  ],
})
export class SigninWelcomePageModule {}
