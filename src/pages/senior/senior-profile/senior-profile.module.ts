import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeniorProfilePage } from './senior-profile';

@NgModule({
  declarations: [
    SeniorProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(SeniorProfilePage),
  ],
})
export class SeniorProfilePageModule {}
