import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginSeniorPage } from '../../auth/login-senior/login-senior';
import { SeniorEditProfilePage } from '../senior-edit-profile/senior-edit-profile';

/**
 * Generated class for the SeniorProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-senior-profile',
  templateUrl: 'senior-profile.html',
})
export class SeniorProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  editProfile() {
    this.navCtrl.setRoot(SeniorEditProfilePage);
  }

  logout() {
    this.navCtrl.setRoot(LoginSeniorPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeniorProfilePage');
  }

}
