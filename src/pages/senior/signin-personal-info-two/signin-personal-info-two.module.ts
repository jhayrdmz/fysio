import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninPersonalInfoTwoPage } from './signin-personal-info-two';

@NgModule({
  declarations: [
    SigninPersonalInfoTwoPage,
  ],
  imports: [
    IonicPageModule.forChild(SigninPersonalInfoTwoPage),
  ],
})
export class SigninPersonalInfoTwoPageModule {}
