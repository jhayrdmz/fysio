import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SigninPersonalInfoPage } from '../signin-personal-info/signin-personal-info';
import { SigninPlanningPage } from '../signin-planning/signin-planning';

/**
 * Generated class for the SigninPersonalInfoTwoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin-personal-info-two',
  templateUrl: 'signin-personal-info-two.html',
})
export class SigninPersonalInfoTwoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goSigninPersonal() {
    this.navCtrl.setRoot(SigninPersonalInfoPage);
  }

  goSigninPlanning() {
    this.navCtrl.setRoot(SigninPlanningPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPersonalInfoTwoPage');
  }

}
