import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { ChatPage } from '../pages/chat/chat';

/*
 * Fysio pages
 */
import { LoginFysioPage } from '../pages/auth/login-fysio/login-fysio';
import { PersonalInfoPage } from '../pages/fysio/personal-info/personal-info';
import { TourPage } from '../pages/fysio/tour/tour';
import { ClientOverviewPage } from '../pages/fysio/client-overview/client-overview';
import { ProfilePage } from '../pages/fysio/profile/profile';
import { ProfileSettingsPage } from '../pages/fysio/profile-settings/profile-settings';
import { EditProfilePage } from '../pages/fysio/edit-profile/edit-profile';
import { EditGoalPage } from '../pages/fysio/edit-goal/edit-goal';
import { AboutPage } from '../pages/fysio/about/about';
import { NotificationsPage } from '../pages/fysio/notifications/notifications';
import { ClientDetailPage } from '../pages/fysio/client-detail/client-detail';
import { ChangeProgramPage } from '../pages/fysio/change-program/change-program';
import { AddExercisePage } from '../pages/fysio/add-exercise/add-exercise';
import { ChangeExercisePage } from '../pages/fysio/change-exercise/change-exercise';
import { AddAppointmentPage } from '../pages/fysio/add-appointment/add-appointment';
import { ChangeWalkProgramPage } from '../pages/fysio/change-walk-program/change-walk-program';
import { OverviewTrainingPage } from '../pages/fysio/overview-training/overview-training';

import { ModalClientPage } from '../pages/fysio/modal-client/modal-client';
import { ModalNewClientPage } from '../pages/fysio/modal-new-client/modal-new-client';
import { ModalChooseProgramPage } from '../pages/fysio/modal-choose-program/modal-choose-program';
import { ModalChooseGoalPage } from '../pages/fysio/modal-choose-goal/modal-choose-goal';
import { ModalChangeProgramPage } from '../pages/fysio/modal-change-program/modal-change-program';
import { ModalChangeWalkProgramPage } from '../pages/fysio/modal-change-walk-program/modal-change-walk-program';
import { ModalControlProgramPage } from '../pages/fysio/modal-control-program/modal-control-program';
import { ModalEmailClientPage } from '../pages/fysio/modal-email-client/modal-email-client';

/*
 * Senior pages
 */
import { LoginSeniorPage } from '../pages/auth/login-senior/login-senior';
import { TourSeniorPage } from '../pages/senior/tour-senior/tour-senior';
import { HomePage } from '../pages/senior/home/home';
import { SchedulePage } from '../pages/senior/schedule/schedule';
import { ScheduleRestdayPage } from '../pages/senior/schedule-restday/schedule-restday';
import { SeniorProfilePage } from '../pages/senior/senior-profile/senior-profile';
import { SeniorEditProfilePage } from '../pages/senior/senior-edit-profile/senior-edit-profile';
import { AllChallengesPage } from '../pages/senior/all-challenges/all-challenges';
import { ExplanationModalPage } from '../pages/senior/explanation-modal/explanation-modal';

import { TrainingPage } from '../pages/senior/training/training';
import { TrainingStartPage } from '../pages/senior/training-start/training-start';
import { TrainingCheckinPage } from '../pages/senior/training-checkin/training-checkin';
import { TrainingWarmupPage } from '../pages/senior/training-warmup/training-warmup';
import { TrainingBalancePage } from '../pages/senior/training-balance/training-balance';
import { TrainingEndPage } from '../pages/senior/training-end/training-end';
import { TrainingNextPage } from '../pages/senior/training-next/training-next';

import { ActivateCodePage } from '../pages/senior/activate-code/activate-code';
import { InstallPasswordPage } from '../pages/senior/install-password/install-password';
import { SigninWelcomePage } from '../pages/senior/signin-welcome/signin-welcome';
import { SigninPersonalInfoPage } from '../pages/senior/signin-personal-info/signin-personal-info';
import { SigninPersonalInfoTwoPage } from '../pages/senior/signin-personal-info-two/signin-personal-info-two';
import { SigninPlanningPage } from '../pages/senior/signin-planning/signin-planning';
import { SigninCompletedPage } from '../pages/senior/signin-completed/signin-completed';

import { ModalForgotPasswordPage } from '../pages/auth/modal-forgot-password/modal-forgot-password';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    MyApp,
    TourPage,

    ModalForgotPasswordPage,
    PersonalInfoPage,
    ClientOverviewPage,
    ProfilePage,
    ProfileSettingsPage,
    EditProfilePage,
    EditGoalPage,
    AboutPage,
    NotificationsPage,
    ClientDetailPage,
    ChangeProgramPage,
    AddExercisePage,
    ChangeExercisePage,
    AddAppointmentPage,
    ChangeWalkProgramPage,
    OverviewTrainingPage,

    ModalClientPage,
    ModalNewClientPage,
    ModalChooseProgramPage,
    ModalChooseGoalPage,
    ModalChangeProgramPage,
    ModalChangeWalkProgramPage,
    ModalControlProgramPage,
    ModalEmailClientPage,

    TourSeniorPage,
    HomePage,
    SchedulePage,
    ScheduleRestdayPage,
    SeniorProfilePage,
    SeniorEditProfilePage,
    AllChallengesPage,
    ExplanationModalPage,

    TrainingPage,
    TrainingStartPage,
    TrainingCheckinPage,
    TrainingWarmupPage,
    TrainingBalancePage,
    TrainingEndPage,
    TrainingNextPage,

    ActivateCodePage,
    InstallPasswordPage,
    SigninWelcomePage,
    SigninPersonalInfoPage,
    SigninPersonalInfoTwoPage,
    SigninPlanningPage,
    SigninCompletedPage,

    LoginFysioPage,
    LoginSeniorPage,
    ChatPage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp, { animate: false })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TourPage,

    ModalForgotPasswordPage,
    PersonalInfoPage,
    ClientOverviewPage,
    ProfilePage,
    ProfileSettingsPage,
    EditProfilePage,
    EditGoalPage,
    AboutPage,
    NotificationsPage,
    ClientDetailPage,
    ChangeProgramPage,
    AddExercisePage,
    ChangeExercisePage,
    AddAppointmentPage,
    ChangeWalkProgramPage,
    OverviewTrainingPage,

    ModalClientPage,
    ModalNewClientPage,
    ModalChooseProgramPage,
    ModalChooseGoalPage,
    ModalChangeProgramPage,
    ModalChangeWalkProgramPage,
    ModalControlProgramPage,
    ModalEmailClientPage,

    TourSeniorPage,
    HomePage,
    SchedulePage,
    ScheduleRestdayPage,
    SeniorProfilePage,
    SeniorEditProfilePage,
    AllChallengesPage,
    ExplanationModalPage,

    TrainingPage,
    TrainingStartPage,
    TrainingCheckinPage,
    TrainingWarmupPage,
    TrainingBalancePage,
    TrainingEndPage,
    TrainingNextPage,

    ActivateCodePage,
    InstallPasswordPage,
    SigninWelcomePage,
    SigninPersonalInfoPage,
    SigninPersonalInfoTwoPage,
    SigninPlanningPage,
    SigninCompletedPage,

    LoginFysioPage,
    LoginSeniorPage,
    ChatPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
